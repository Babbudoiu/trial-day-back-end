const express = require("express");
const userRouter = express.Router();
const {createUser, authUser, findUser} = require("./users.controllers");
const {auth} = require("../middleware");

userRouter.post("/users", createUser);
userRouter.get("/users", auth, authUser);
userRouter.post("/users/:username", findUser);

module.exports = userRouter;