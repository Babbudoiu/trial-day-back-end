const mongoose = require("mongoose");

const postsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    tag: {
        type: String,
        required: true
    }
});

const Posts = mongoose.model("Posts", postsSchema);
module.exports = Posts;

