const Post = require("./posts.model");

exports.createPost = async (req, res) => {
    try {
        const post = new Post(req.body);
        const savedPost = await post.save();
        res.status(200).send({ post: savedPost, message: "Post created" })
    } catch (error) {
        res.status(500).send(error)
    }
};

exports.findPost = async (req, res) => {
    try {
        const title = req.params.title;
        const targetPost = await Post.findOne({ title: title });
        res.status(200).send({ post: targetPost, message: "Post found" })
    } catch (error) {
        res.status(500).send(error)
    }
};

exports.deletePost = async (req, res) => {
    try {
        const title = req.params.title;
        const removePost = await Post.findOneAndDelete({ title: title });
        res.status(200).send({ post: removePost, message: "Post removed" })
    } catch (error) {
        res.status(500).send(error)
    }
};

exports.updatePost = async (req, res) => {
    try {
        const modifyPost = await Post.findOneAndUpdate({ title: req.body.title }, { text: req.body.text });
        res.status(200).send({ post: modifyPost, message: "Post updated" });
    } catch (error) {
        res.status(500).send(error)
    }
};
