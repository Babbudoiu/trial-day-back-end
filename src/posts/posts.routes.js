const express = require("express");
const postRouter = express.Router();
const { createPost, findPost, updatePost, deletePost } = require("./posts.controllers");

postRouter.post("/posts", createPost);
postRouter.get("/posts/:title", findPost);
postRouter.delete("/post/:title", deletePost);
postRouter.put("/posts", updatePost);

module.exports = postRouter;